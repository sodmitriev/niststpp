#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
               O V E R L A P P I N G  T E M P L A T E  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
OverlappingTemplateMatchings(const std::vector<bool>& seqtest,
							 const std::vector<bool>& seqtempl,
							 size_t blockSize)
{
	size_t M = blockSize;
	size_t N = seqtest.size()/M;
	std::vector<unsigned int> Wj(N, 0);

	double lambda = static_cast<double>(M - seqtempl.size() + 1) / pow(2, seqtempl.size());
	if (lambda <= 0)
	{
		throw std::logic_error("Lambda (" + std::to_string(lambda) + ") not being positive!");
	}

	double varWj = M*(1.0/pow(2.0, seqtempl.size()) - (2.0*seqtempl.size()-1.0)/pow(2.0, 2.0*seqtempl.size()));

	for (int i=0; i<N; i++ ) {
        unsigned int W_obs = 0;
		for (int j=0; j<M-seqtempl.size()+1; j++ ) {
			bool match = true;
			for (int k=0; k<seqtempl.size(); k++ ) {
				if ( seqtempl[k] != seqtest[i*M+j+k] )
                {
                    match = false;
                    break;
                }
			}
			if ( match )
				W_obs++;
		}
		Wj[i] = W_obs;
	}
	double chi2 = 0.0;
	for (int i=0; i<N; i++ ) {
		chi2 += pow(Wj[i] - lambda, 2) / varWj;
	}
	return cephes_igamc(N/2.0, chi2/2.0);
}

