#include <cmath>

#include "cephes.h"
#include "stat_fncs.h"

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
		    C U M U L A T I V E  S U M S  T E S T
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

double
CumulativeSums(const std::vector<bool>& seq, bool backwards)
{
	double	sum1 = 0.0, sum2 = 0.0;

	int S = 0;
	int sup = 0;
	int inf = 0;
	int z = -inf;
    static const int zero = 0;
    const int * sub = backwards? &S : &zero;
    for (auto i : seq)
    {
        i ? S++ : S--;
        if ( S > sup )
            sup++;
        if ( S < inf )
            inf--;
        z = (sup-*sub > *sub-inf) ? sup-*sub : *sub-inf;
    }
    for (auto k = (-static_cast<int>(seq.size()) / z + 1) / 4; k <= (static_cast<int>(seq.size()) / z - 1) / 4; k++)
    {
        sum1 += cephes_normal(((4 * k + 1) * z) / sqrt(seq.size()));
        sum1 -= cephes_normal(((4 * k - 1) * z) / sqrt(seq.size()));
    }
    for (auto k = (-static_cast<int>(seq.size()) / z - 3) / 4; k <= (static_cast<int>(seq.size()) / z - 1) / 4; k++)
    {
        sum2 += cephes_normal(((4 * k + 3) * z) / sqrt(seq.size()));
        sum2 -= cephes_normal(((4 * k + 1) * z) / sqrt(seq.size()));
    }
	return 1.0 - sum1 + sum2;
}